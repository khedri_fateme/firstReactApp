import React from "react";
class Container extends React.Component {
  render() {
    return (
      <div className=" mt-5 container">
        <div className="row  ">
          <div style={{height:"70px"}} className="col-9 border bg-warning">1</div>
          <div style={{height:"300px"}} className="col-8 border bg-info">2</div>
          <div style={{height:"300px"}} className="col-4 border bg-info">3</div>
        </div>

        {/* <div className="row"></div> */}

        <div className="justify-content-center row">
          <div className="col-8">
            <div  style={{height:"70px"}} className="col-md-auto border bg-danger">4</div>
          </div>
        </div>
        <div className="row justify-content-center">
          <div style={{height:"70px"}} className="col-3 border bg-success">5</div>
        </div>
      </div>
    );
  }
}
export default Container;
